/**Tip Calculator
 * @author Lorenzo Pedroza (based on tutorial by Paulo Dichone)
 * */

package com.tipcalculator.l2.lpedr.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText enteredAmount;
    private TextView showTipEnteredInSeekBar;
    private SeekBar seekBar;
    private Button calculateButton;
    private TextView tipTextView;
    private TextView showBillTextView;
    private ToggleButton showBillToggleButton;
    private int seekbarPercentage = 0;
    private float enteredBillFloat = 0.0f;
    private float tip = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enteredAmount = findViewById(R.id.enterBillTotalEditText);
        seekBar = findViewById(R.id.inputTipPercentageSeekBarID);
        calculateButton = findViewById(R.id.calculateButtonID);
        tipTextView = findViewById(R.id.tipTextViewID);
        showTipEnteredInSeekBar = findViewById(R.id.tipPercentagefromSeekBarTextViewID);
        showBillToggleButton = findViewById(R.id.showBillToggleID);
        showBillTextView = findViewById(R.id.totalBillTextViewID);

        calculateButton.setOnClickListener(this);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                showTipEnteredInSeekBar.setText(String.valueOf(seekBar.getProgress())+ " %");
                showBillToggleButton.setVisibility(View.INVISIBLE); //Bill is different when tip is changed. So hide previous results.
                tipTextView.setVisibility(View.INVISIBLE); //also tip will be different, so hide it.
                showBillToggleButton.setVisibility(View.INVISIBLE); //and also hide button for now.
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekbarPercentage = seekBar.getProgress();
            }
        });

        showBillToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    showBillTextView.setVisibility(View.VISIBLE);
                    showBillTextView.setText("Total bill is $" + String.valueOf(enteredBillFloat + tip));
                }
                else{
                    showBillTextView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        calculate();
    }

    public void calculate(){
        if(!enteredAmount.getText().toString().equals("")){//check for value entered. Check that it's not blank.
            enteredBillFloat = Float.parseFloat(enteredAmount.getText().toString());
            tip = enteredBillFloat * (seekbarPercentage/100.0f); //need to divide by float. Otherwise always get 0.0
            tipTextView.setVisibility(View.VISIBLE); //make tip visible
            tipTextView.setText("Your tip will be $" + String.valueOf(tip));
            showBillToggleButton.setVisibility(View.VISIBLE);//NOW give them option to see total bill after valid value is entered.
        }
        else{
            Toast.makeText(MainActivity.this,"Please enter a bill amount", Toast.LENGTH_LONG).show();
        }
    }
}
